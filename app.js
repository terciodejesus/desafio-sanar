var express = require('express');
var expressValidator = require('express-validator');

var contestRouter = require('./routes/contest');

var app = express();

var customDate = require('./lib/customDate');
var customArray = require('./lib/customArray');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(expressValidator({
  customValidators: {
    isValidDate: customDate.isValidDate,
    greaterDate: customDate.greaterDate,
    isArray: customArray.isArray
  }
}));

app.use('/', contestRouter);

app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'), function() {
  console.log('Server started on port ' + app.get('port'));
});
