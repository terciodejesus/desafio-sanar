exports.isValidDate = function(value) {
  if (!value.match(/^\d{4}-\d{2}-\d{2}$/)) return false;

  const customDate = new Date(value);
  if (!customDate.getTime()) return false;
  return customDate.toISOString().slice(0, 10) === value;
}

exports.greaterDate = function(value, date) {
  if (!value.match(/^\d{4}-\d{2}-\d{2}$/)) return false;
  if (!date.match(/^\d{4}-\d{2}-\d{2}$/)) return false;

  const customDate1 = new Date(value);
  const customDate2 = new Date(date);

  if (!customDate1.getTime()) return false;
  if (!customDate2.getTime()) return false;

  return customDate1.getTime() >= customDate2.getTime();
}
