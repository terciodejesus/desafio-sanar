var fs = require('fs');

exports.save = function(req, res) {
  var list = [];
  var json = req.body;

  fs.stat('contests.txt', function(err) {
    if (err) {
      list.push(json);
      fs.writeFile('contests.txt', JSON.stringify(list), function(err) {
        if (err) { res.send({ msg: "Error on write file" }); }
      });
    } else {
      fs.readFile('contests.txt', function(err, data) {
        if (err) {
          res.send({ msg: "Error on read file" });
        } else {
          var jsonFile = JSON.parse(data);

          jsonFile.forEach(e => list.push(e));

          list.push(json);

          fs.writeFile('contests.txt', JSON.stringify(list), function(err) {
            if (err) { res.send({ msg: "Error on write file" }); }
          });
        }
      });
    }
  });
  res.send(req.body);
};

exports.find = function(req, res) {
  var list = [];

  fs.stat('contests.txt', function(err) {
    if (err) {
      res.send({ msg: "File not found" });
    } else {
      fs.readFile('contests.txt', function(err, data) {
        if (err) {
          res.send({ msg: "Error on read file" });
        } else {
          var jsonFile = JSON.parse(data);

          if (req.query) {
            if (req.query.profissao) {
              jsonFile = jsonFile.filter(function(e) {
                for (var j = 0; j < e.vagas.length; j++) {
                  if (e.vagas[j]["profissao"].toUpperCase() == req.query.profissao.toUpperCase()) {
                    return e;
                  }
                }
              });
            } else {
              for (var k in req.query) {
                jsonFile = jsonFile.filter(e => e[k].toUpperCase() == req.query[k].toUpperCase())
              }
            }
          }
          res.send(jsonFile);
        }
      });
    }
  });
}
