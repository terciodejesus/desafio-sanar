var express = require('express');
var router = express.Router();

var contest_controller = require('../controllers/contestController');

router.get('/', contest_controller.index);
router.post('/contest/create', contest_controller.create);
router.get('/contest/show', contest_controller.show)

module.exports = router;
