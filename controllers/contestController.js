var contest = require('../models/contest');
const { check, validationResult } = require('express-validator/check');

exports.create = function(req, res) {
  var data = req.body;

  req.checkBody('titulo', 'Title is required').notEmpty();
  req.checkBody('titulo', 'Title must be at least 120 characters').len(1, 120);

  req.checkBody('ano', 'Ano is required').notEmpty();
  req.checkBody('ano', 'Ano must be numeric').isNumeric();
  req.checkBody('ano', 'Ano should be 04 digits').len(4, 4);

  req.checkBody('status', 'Status is required').notEmpty();
  req.checkBody('status', "Status must be 'aberto' or 'fechado' or 'cancelado'").isIn(['aberto', 'fechado', 'cancelado']);

  req.checkBody('inicio_inscricoes', 'inicio_inscricoes is required').notEmpty();
  req.checkBody('inicio_inscricoes', 'inicio_inscricoes must be a date').isValidDate();

  req.checkBody('final_inscricoes', 'final_inscricoes is required').notEmpty();
  req.checkBody('final_inscricoes', 'final_inscricoes must be a date').isValidDate();
  req.checkBody('final_inscricoes', "'final_inscricoes' and 'inicio_inscricoes' are required or 'final_inscricoes' should be higher than 'inicio_inscricoes'").greaterDate(data.inicio_inscricoes);

  req.checkBody('banca', 'Banca is required').notEmpty();
  req.checkBody('banca', 'Banca must be at leas 50 characters').len(1, 50);

  req.checkBody('vagas', 'vagas is required').isArray();

  if (req.body.vagas && req.body.vagas.length > 0) {
    for (var i = 0; i < req.body.vagas.length; i++) {
      job = "vagas" + "[" + i + "]" + "[profissao]";
      req.checkBody(job, 'Profissao is required').notEmpty();
      req.checkBody(job, 'Profissao must be the following values').isIn(['Dentista', 'Enfermeiro', 'Fisioterapeuta', 'Medico', 'Nutrioncista', 'Psicologo', 'Veterinario']);

      quantity = "vagas" + "[" + i + "]" + "[qtd]";
      req.checkBody(quantity, 'Qtd is required').notEmpty();
      req.checkBody(quantity, 'Qtd must be at least 3 characters').len(1, 3);
    }
  }

  var errors = req.validationErrors();

  if (errors) {
    res.send(errors);
  } else {
    contest.save(req, res);
  }
};

exports.show = function(req, res) {
  contest.find(req, res);
}

exports.index = function(req, res) {
  res.render('index', res);
}
